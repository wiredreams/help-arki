﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class JeromesMessaging : MonoBehaviour {

    // Updates text every 10 updates
    public int TextSpeed = 10;
    public string currentMessage;
    public Text text;
    public bool typing = false;

    public float NextMessageTiming = 3f;

    private int updatecounter = 0;
    private string oldmessage;
    private int currentCharNumber = 1;
    private bool force = false;

    void Start()
    {
        oldmessage = currentMessage;
    }

    void Update()
    {
        if (messageChanged())
        {
            currentCharNumber = 1;
            text.text = " ";
            force = true;
        }
        if ((force) || updatecounter < TextSpeed || currentCharNumber > currentMessage.Length)
        {
            updatecounter++;
            if (currentCharNumber >= currentMessage.Length)
            {
                typing = false;
            }
            return;
        }
        else
        {
            updatecounter = 0;
            addtext();
            typing = true;
        }
    }

    private void addtext()
    {
        try
        {
            text.text = oldmessage.Substring(0, currentCharNumber);
        }
        catch { }
        currentCharNumber++;
    }

    bool messageChanged()
    {
        if (currentMessage.ToLower().Equals(oldmessage.ToLower()))
        {
            return false;
        }
        else
        {
            currentCharNumber = 1;
            oldmessage = currentMessage;
            return false;
        }
    }

}
