﻿using UnityEngine;
using System.Collections;

public class StartArkiScript : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        GetComponent<Animator>().SetTrigger("Win Trigger");
    }

    void wait(float time)
    {
        waiting(time);
    }

    private IEnumerator waiting(float time)
    {
        yield return new WaitForSeconds(time);
    }


}
