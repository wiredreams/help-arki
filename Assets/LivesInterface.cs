﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LivesInterface : MonoBehaviour {

    public List<Image> Lives;

    private int numberofLives = 4;
    
    public void decreaseLives()
    {
        Lives[numberofLives].GetComponent<Animator>().SetBool("dead", true);
        numberofLives--;
    }

    public int GetLives()
    {
        return numberofLives;
    }



    public void Reset()
    {
        numberofLives = Lives.Count - 1;
        foreach(Image heart in Lives)
        {
            if(heart.GetComponent<Animator>().GetBool("dead"))
            {
                heart.GetComponent<Animator>().SetBool("dead", false);
                heart.rectTransform.localScale = new Vector3(1, 1, 1);
            }
        }
    }
	
}
