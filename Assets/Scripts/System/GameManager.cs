﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public Canvas startCanvas;
    public Canvas preGameCanvas;
    public Canvas multipleChoiceCanvas;
    public Canvas specializationCanvas;
    public Canvas GameOverCanvas;

    public List<MultipleChoiceQuestion> MultipleChoiceQuestions;
    public List<SpecializationQuestion> SpecializationQuestions;

    public Dictionary<QuestionType, List<int>> Limiters;
    public int Score;
    public int Lives = 3;

    #region "Before Question"
    // Use this for initialization
    void Start () {
        Limiters.Add(QuestionType.MultipleChoice, new List<int>());
        Limiters.Add(QuestionType.Specialization, new List<int>());
        multipleChoiceCanvas.enabled = false;
        specializationCanvas.enabled = false;
        startCanvas.enabled = true;
	}

    public void onPlayClick()
    {
        int[] questionNums = ChooseQuestion();
        ProcessQuestion(questionNums[0], questionNums[1]);
    }

    public int[] ChooseQuestion()
    {
        //Gets question type
        int questionTypeNumber;
        int maxrange;
        int questionNumber;
        int[] x = { -1, -1 };
        while (true)
        {
            questionTypeNumber = Random.Range(0, 1);
            if (questionTypeNumber == 0)
                maxrange = MultipleChoiceQuestions.Count;
            else
                maxrange = SpecializationQuestions.Count;
            questionNumber = Random.Range(0, maxrange);
            x[0] = questionTypeNumber;
            x[1] = questionNumber;
            if (isNeverAskedQuestion(questionTypeNumber, questionNumber))
                break;
        }
        return x;
    }

    private bool isNeverAskedQuestion(int questionTypeNumber, int questionNumber)
    {
        QuestionType questionType = intToQuestionType(questionTypeNumber);
        return Limiters[questionType].Contains(questionNumber);
    }

    private QuestionType intToQuestionType(int questionTypeNumber)
    {
        if (questionTypeNumber == 0)
            return QuestionType.MultipleChoice;
        else
            return QuestionType.Specialization;
    }

    public void ProcessQuestion(int QuestionTypeNumber, int QuestionNumber)
    {
        //TODO: Assigning text to the buttons
        Question question = GetQuestion(QuestionTypeNumber, QuestionNumber);
        Debug.Log("Question Type " + QuestionTypeNumber + " Question Number " + QuestionNumber);

        if (question.GetType() == typeof(MultipleChoiceQuestion))
        {
            
        }
        else
        {

        }
    }

    public Question GetQuestion(int QuestionTypeNumber, int QuestionNumber)
    {
        QuestionType questionType = intToQuestionType(QuestionTypeNumber);
        if(questionType == QuestionType.MultipleChoice)
            return MultipleChoiceQuestions[QuestionNumber];
        else
            return SpecializationQuestions[QuestionNumber];
    }
    #endregion
    #region "AfterQuestion"

    public void ProcessAnswer(string answer)
    {

    }

    public void UpdateLives()
    {

    }

    public void GameOver()
    {

    }

    
    #endregion

    public void onClickAnswer(string answer)
    {
        
    }

}

public enum QuestionType
{
    MultipleChoice,
    Specialization
}

[System.Serializable]
public class MultipleChoiceQuestion : Question
{
    public string A;
    public string B;
    public string C;
    public string D;
}

[System.Serializable]
public class SpecializationQuestion : Question
{
}

public class Question
{
    public int timeLimit;
    public string questionText;
    public string answer;
}