﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuestionInterface : MonoBehaviour {

    public string rightAnswer;
    public RevampedGameManager gameManager;
    public float timeLimitInSeconds;
    public string startmessage;
    public string rightmessage;
    public string wrongmessage;
    public string timeoutmessage;
    public Text timerText;

    private int internalClock;

    public void onClickAnswer(string answer)
    {
        if (rightAnswer.ToLower().Equals(answer.ToLower()))
            onRightAnswer();
    }

    private void onRightAnswer()
    {
        gameManager.RightAnswer();
    }

    private void onWrongAnswer()
    {
        gameManager.WrongAnswer();
    }

    private void onTimeout()
    {
        gameManager.WrongAnswer();
    }

    void Start()
    {
        timerText.text = Mathf.RoundToInt(timeLimitInSeconds).ToString();
        internalClock = Mathf.RoundToInt(timeLimitInSeconds);
        StartCoroutine(timer());
    }

    public IEnumerator timer()
    {
        while(internalClock > 0){
            yield return new WaitForSeconds(1f);
            internalClock--;
            timerText.text = internalClock.ToString();
        }
        gameManager.callTimeout();
    }
}
