﻿using UnityEngine;
using System.Collections;

public class ArkiMainScript : MonoBehaviour {

    public float x = 3.02f;
    public float speed = 1f;
    public bool exiting = false;
	
	// Update is called once per frame
	void Update () {
	    if(gameObject.transform.position.x > x + 0.01)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed * 10);
        }
        else if (exiting)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed * 100);
        }
	}

    public void Exit()
    {
        exiting = true;
    }
}
