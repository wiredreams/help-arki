﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RevampedGameManager : MonoBehaviour {

    private static bool firstTimePlay = true;

    public List<QuestionInterface> Questions;
    public Canvas StartUI;
    public Canvas GameOverUI;
    public Canvas HowToUI;
    public Canvas messagingUI;
    public JeromesMessaging messaging;
    public GameObject Arki;
    public GameObject Mother;

    public Canvas Right;
    public Canvas Wrong;
    public Canvas TimeOut;
    public GameObject WinUI;
    public Canvas congratulations;

    public GameObject startScene;
    public GameObject mainScene;
    public LivesInterface Lives;

    public StartArkiScript arkiStartScript;
    // Privates    
    private int currentQuestionNumber = -1;
    private Animator boyanimator;
    private Animator girlanimator;
    private List<int> questionsAsked = new List<int>();

	// Use this for initialization
	void Start ()
    {
        if (firstTimePlay)
        {
            SanityProcedure();
            startScene.SetActive(true);
            StartUI.gameObject.SetActive(true);
        }
        else
        {
            startScene.SetActive(false);
            messagingUI.gameObject.SetActive(false);
            GetComponent<AudioSource>().volume = 0.2f;
            mainScene.SetActive(true);
            StartCoroutine(MainScenario());
        }
	}

    void SanityProcedure()
    {
        //return;
        //Close all other UI's
        foreach(QuestionInterface ui in Questions)
        {
            ui.gameObject.SetActive(false);
        }

        //GameOverUI.gameObject.SetActive(false);
        mainScene.SetActive(false);
        Lives.gameObject.SetActive(false);
        foreach (QuestionInterface x in Questions)
        {
            x.gameObject.SetActive(false);
        }
        GetComponent<AudioSource>().volume = 1f;

        Lives.gameObject.SetActive(true);
        Lives.Reset();
    }

    void GameStartProcedures()
    {
        
    }

    #region "Questions"

    private QuestionInterface currentQuestion;
    private string currentRightAnswer;

    IEnumerator NextQuestion()
    {
        int x = questionsAsked.Count;
        if (Questions.Count == questionsAsked.Count)
        {
            //currentQuestion.gameObject.SetActive(false);
            congratulations.gameObject.SetActive(true);
            messagingUI.gameObject.SetActive(false);
            
            yield return new WaitForSeconds(1.5f);
            congratulations.gameObject.SetActive(false);

            messagingUI.gameObject.SetActive(true);
            messaging.currentMessage = "Yay! I won! I can now go to my friend's birthday!";
            boyanimator.SetBool("Talking", true);
            yield return new WaitForSeconds(2f);
            
            boyanimator.SetBool("Talking", false);
            yield return new WaitForSeconds(4f);

            boyanimator.SetBool("Talking", true);
            messaging.currentMessage = "Mom! I Will go now!";
            boyanimator.SetTrigger("Win Trigger");
            yield return new WaitForSeconds(2f);
            boyanimator.SetBool("Talking", false);
            messagingUI.gameObject.SetActive(false);
            yield return new WaitForSeconds(4f);
            Arki.GetComponent<ArkiMainScript>().Exit();
            yield return new WaitForSeconds(3f);

            mainScene.gameObject.SetActive(false);
            WinUI.gameObject.SetActive(true);

        }
        else if(Lives.GetLives() == -1)
        {
            StartCoroutine(GameOver());
        }
        else
        {
            while (true)
            {
                currentQuestionNumber = GetQuestionNumber();
                if (!questionsAsked.Contains(currentQuestionNumber))
                {
                    break;
                }
            }
            currentQuestion = Questions[currentQuestionNumber];
            currentRightAnswer = currentQuestion.rightAnswer;
            currentQuestion.gameObject.SetActive(true);

            messaging.currentMessage = currentQuestion.startmessage;
            girlanimator.SetBool("Talking", true);
            yield return new WaitForSeconds(3f);
            girlanimator.SetBool("Talking", false);
            questionsAsked.Add(currentQuestionNumber);
        }
    }

    public IEnumerator GameOver()
    {
        messaging.currentMessage = "Im sorry mom I haven't studied enough. But please let me go to the birthday party!";
        boyanimator.SetTrigger("Sad");
        boyanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(3f);
        boyanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(4f);

        girlanimator.SetBool("Talking", true);
        messaging.currentMessage = "NOOO!";
        yield return new WaitForSeconds(0.5f);
        girlanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(2f);

        girlanimator.SetBool("Talking", true);
        messaging.currentMessage = "Go back to your room and study! Come back when you can answer my questions!";
        yield return new WaitForSeconds(3f);
        girlanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(4f);

        messagingUI.gameObject.SetActive(false);
        mainScene.gameObject.SetActive(false);
        GameOverUI.gameObject.SetActive(true);
    }

    public int GetQuestionNumber()
    {
        return Random.Range(0, Questions.Count);
    }

    public void OnClickAnswer(string answer)
    {
        if (answer.ToLower().Equals(currentRightAnswer.ToLower()))
            StartCoroutine(RightAnswer());
        else
            StartCoroutine(WrongAnswer());
    }

    public IEnumerator onTimeout()
    {
        currentQuestion.gameObject.SetActive(false);
        TimeOut.gameObject.SetActive(true);
        messagingUI.gameObject.SetActive(false);
        Lives.decreaseLives();
        yield return new WaitForSeconds(1.5f);
        TimeOut.gameObject.SetActive(false);
        messagingUI.gameObject.SetActive(true);

        messaging.currentMessage = currentQuestion.timeoutmessage;
        girlanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(3f);
        girlanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(4f);
        StartCoroutine(NextQuestion());
    }

    public void callTimeout()
    {
        StartCoroutine(onTimeout());
    }


    public IEnumerator RightAnswer()
    {
        currentQuestion.gameObject.SetActive(false);
        Right.gameObject.SetActive(true);
        messagingUI.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        Right.gameObject.SetActive(false);
        messagingUI.gameObject.SetActive(true);

        messaging.currentMessage = currentQuestion.rightmessage;
        girlanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(1.5f);
        girlanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(4f);
        StartCoroutine(NextQuestion());
    }

    public IEnumerator WrongAnswer()
    {

        currentQuestion.gameObject.SetActive(false);
        Wrong.gameObject.SetActive(true);
        messagingUI.gameObject.SetActive(false);
        Lives.decreaseLives();
        yield return new WaitForSeconds(1.5f);
        Wrong.gameObject.SetActive(false);
        messagingUI.gameObject.SetActive(true);

        messaging.currentMessage = currentQuestion.wrongmessage;
        girlanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(1.5f);
        girlanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(4f);
        StartCoroutine(NextQuestion());
    }

    #endregion

    public void onClickPlay()
    {
        StartCoroutine(IntroductionCode());
    }

    public IEnumerator IntroductionCode()
    {        
        StartUI.gameObject.SetActive(false);
        messagingUI.gameObject.SetActive(true);
        messaging.currentMessage = "Hi there! I am Arki. My mother is a math professor and she knows that I am having a hard time in math.";
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", true);
        
        yield return new WaitForSeconds(3.3f);
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", false);
        yield return new WaitForSeconds(5f);
        ///============================
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", true);
        arkiStartScript.gameObject.GetComponent<Animator>().SetTrigger("Sad");
        messaging.currentMessage = "It's my best friend's birthday later but there are still 18 math algebra questions and problems I can't answer.";
        yield return new WaitForSeconds(3.3f);
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", false);
        yield return new WaitForSeconds(5f);

        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", true);
        arkiStartScript.gameObject.GetComponent<Animator>().SetTrigger("Idle");
        messaging.currentMessage = "My mom told me that if I can't answer it correctly she won't allow me to go to my best friend's birthday party.";

        yield return new WaitForSeconds(3.3f);
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", false);
        yield return new WaitForSeconds(5f);

        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", true);
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Smiling", true);
        messaging.currentMessage = "I'm glad you will help me! Let's start the challenge now and goodluck!";

        yield return new WaitForSeconds(2f);
        arkiStartScript.gameObject.GetComponent<Animator>().SetBool("Talking", false);
        yield return new WaitForSeconds(5f);

        startScene.SetActive(false);
        messagingUI.gameObject.SetActive(false);
        GetComponent<AudioSource>().volume = 0.2f;
        mainScene.SetActive(true);
        StartCoroutine( MainScenario());
    }

    public IEnumerator MainScenario()
    {

        boyanimator = Arki.GetComponent<Animator>();
        girlanimator = Mother.GetComponent<Animator>();

        yield return new WaitForSeconds(2f);

        messaging.currentMessage = "Mom! I will go out to go to my classmate's birthday party!";
        messagingUI.gameObject.SetActive(true);
        boyanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(5f);
        boyanimator.SetBool("Talking", false);

        girlanimator.SetBool("Talking", true);
        messaging.currentMessage = "Oh no you can't! You must first show me if you learned something from algebra class!";
        yield return new WaitForSeconds(3f);
        girlanimator.SetBool("Talking", false);
        yield return new WaitForSeconds(4f);

        messaging.currentMessage = "Mom! I learned lots of stuff!";
        boyanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(2f);
        boyanimator.SetBool("Talking", false);

        messaging.currentMessage = "Are you sure? Then let me ask you some questions then!";
        girlanimator.SetBool("Talking", true);
        yield return new WaitForSeconds(2f);
        girlanimator.SetBool("Talking", false);

        StartCoroutine(NextQuestion());
    }

    public void onClickHowTo()
    {
        StartUI.gameObject.SetActive(false);
        HowToUI.gameObject.SetActive(true);
    }

    public void onClickHowToOk()
    {
        StartUI.gameObject.SetActive(true);
        HowToUI.gameObject.SetActive(false);
    }

    void wait(float time)
    {
        StartCoroutine(waiting(time));
    }

    private IEnumerator waiting(float time)
    {
        yield return new WaitForSeconds(time);
    }

    public void onCLickPlayAgain()
    {
        RevampedGameManager.firstTimePlay = false;
        Application.LoadLevel(Application.loadedLevel);
    }
}
